---
layout: markdown_page
title: "UX Research"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## UX Research

The goal of UX Research at GitLab is to connect with GitLab users all around the world and gather insight into their behaviors, motivations and goals when using GitLab. These insights are then used to inform and strengthen product and design decisions.

### How to request research

1. Create a new issue using the `Research proposal` template in the [UX research project](https://gitlab.com/gitlab-org/ux-research) and `@` mention the relevant UX Researcher, UX Designer, and Product Manager for the product stage. Ensure you answer all questions outlined in the `Research proposal` template.

    * You can find out who the relevant UX Researcher and/or UX Designer is by looking at the [team page](/team/) and filtering by the `UX` department. To find out who the relevant Product Manager is, take a look at the [Product category page](/handbook/product/categories/).

    * Anybody across GitLab can raise a research proposal, this includes UX Researchers.

1. The UX Researcher will review the issue and may respond with some follow-up questions. 

    * We want to lessen the time that researchers spend within issues and/or Slack soliciting research requirements. As a rule of thumb, if we have gone back and forth more than 3 times, it's time for a video call. 

    * Similarly, some features are more complex than others. A UX Researcher needs to fully understand the feature they are testing and why they are testing it. Sometimes, it's much easier to get a grasp on the feature's history, your existing plans and plans for the future when you talk with us. In cases such as these, the UX Researcher will schedule a kick-off call with all relevant stakeholders (including the UX Designer and Product Manager) to run through the research proposal. 

1. In collaboration with the Product Manager, the UX Researcher will determine the priority of the study and schedule it accordingly.

1. The UX Researcher will book a wash-up meeting with all relevant stakeholders when the results of the study are available. 

### Wash-up meetings

1.  During a wash-up meeting, all stakeholders collaboratively agree upon a list of improvements/actions to take forward. UX Researchers are responsible for scheduling wash-up meetings. At least 24 hours prior to the wash-up meeting, a Google document containing the research study’s key findings will be added to the meeting’s calendar invitation. 

1. In order to ensure that the meeting is an effective use of everybody’s time, you should familiarise yourself with the study’s key findings prior to the meeting and come prepared with suggestions.


### How to find previous research

As of November 2018, all research conducted by GitLab's UX Researchers is documented within a blog post via the [GitLab blog](https://about.gitlab.com/blog/). 


### How to create a design evaluation (guidance for UX Designers)

1. Create a new issue using the `Design evaluation` template in the [UX research project](https://gitlab.com/gitlab-org/ux-research) and `@` mention the relevant UX Researcher and Product Manager for the product stage. 

1. Update the `Design evaluation` with the following:
    * Add as much information as you can to the issue.
    * Label the issue with the area of GitLab you’re testing (for example, `navigation`), the status of the issue (`in progress`) and the Product stage (for example, `manage`).
    * Mark the issue as `confidential` until the research is completed so it doesn’t influence user behavior.
    * Assign the issue to yourself.

1. The UX Researcher will quickly review the issue and advise whether it is feasible to send the study during the time frame you have requested (Occasionally, we may need to stagger when the study is sent, since we do not want to bombard users with research studies). The UX Researcher will add a milestone to the issue.

1. Create your test in [UsabilityHub](https://usabilityhub.com/). UX Designers share a login for UsabilityHub. The credentials are stored in 1Password. Some tips for creating a test:
    * Use a descriptive test name. Make sure it includes the issue number of the research proposal.
    * We don't incentivise users to participate in design evaluations. In order to encourage participation, keep the test length under 10 minutes (UsabilityHub will provide you with an estimated completion time as you build your test).
    * Try to avoid [priming users](https://www.nngroup.com/articles/priming/). For example, when writing tasks for a first click or navigation test, where possible, avoid using terms that appear in the interface or which give the answer away. 
    * Always question whether you are using the right research methodology. For example, if you are asking lots of straight-up questions, would it be best to ditch the design evaluation and write a survey instead? Check out the [UsabilityHub guides](https://usabilityhub.com/guides) for common use cases for each methodology.
    * Remember what users say and what they do can be very different. Just because users prefer a design, doesn't mean they can use it. Find out more about [when you should use preference tests](https://www.nngroup.com/articles/first-rule-of-usability-dont-listen-to-users/).
    * Read more about how to [test visual design](https://www.nngroup.com/articles/testing-visual-design/).
    * If you're ever in doubt when creating your test, reach out to a UX Researcher!

1. When your test is ready, update the `Design evaluation` issue with any outstanding information and ping the relevant UX Researcher for final review. 

1. Once you and the UX Researcher are happy with the test. The UX Researcher will distribute the test to subscribers of GitLab First Look.

1. When you and the UX Researcher feel that sufficient data has been collected. The test can be closed in UsabilityHub.

1. You are responsible for analyzing any tests you have created. You are always welcome to reach out to a UX Researcher for assistance. 

1. Document the study within a [blog post](/handbook/marketing/blog/#create-a-new-post). The blog post should consist of:
    * Research hypotheses
    * Research methodology
    * Findings
    * Next steps/recommendations.

1. Update the `Design evaluation` issue with the following:
    * Link to the blog post.
    * Unmark the issue as `confidential`. 
    * Update the status of the issue to `done`.
    * Close the issue. You should stay assigned to closed issues so it's obvious who completed the research.

### How to review a design evaluation (guidance for UX Researchers)

1. UX Researchers should familiarise themselves with the process outlined in [How to create a design evaluation (guidance for UX Designers)](/handbook/engineering/ux/ux-research/#how-to-create-a-design-evaluation-guidance-for-ux-designers)

1. When sending a design evaluation to GitLab First Look users, ensure standard processes are followed. For example, distributing the study to a test sample of users, sending the study to GitLab First Look subscribers who have opted in to design evaluations from the relevant product stage, etc. 

### GitLab First Look

GitLab First Look (formerly the UX Research Panel) is a group of users that have opted in to receive research studies from GitLab. GitLab First Look is managed and maintained by the UX Research team. To find out more or to join, please visit [GitLab First Look](/community/gitlab-first-look/index.html).

## UX Researchers

### UX Researcher onboarding

If you are just starting out here at GitLab, welcome! Make sure to review all the pages here in the UX Research section of the handbook, they will help you get oriented. There is also a specific page dedicated to [UX Researcher onboarding](/handbook/engineering/ux/uxresearcher-onboarding).

### UX Researcher tools

All UX Researchers have individual accounts for the following tools:

[SurveyMonkey](https://www.surveymonkey.com/) - All UX Researchers should utilise SurveyMonkey for surveys. Please use the `GitLab Theme` to style your survey. The GitLab logo should be positioned in the top left hand corner of every page (applied automatically, resize to `small`). 

[UsabilityHub](https://usabilityhub.com/) - Used to build design evaluations, such as first click tests, preference tests and five second tests. UsabilityHub should not be used for surveys. 

[MailChimp](https://mailchimp.com) - Used to send campaigns to subscribers of GitLab First Look.

[OptimalWorkshop](https://www.optimalworkshop.com) - Used for card sorts and tree testing. We do not have an ongoing subscription to OptimalWorkshop. We purchase a monthly license as and when required. 

[Zoom Pro Account](https://zoom.us/) - We use Zoom to run usability testing sessions and user interviews.

### How we decide what to research

* UX Researchers are encouraged to sit in on monthly planning calls for their relevant product stages.  These meetings keep them informed about what Product feels are the most important initiatives at GitLab. UX Researchers should offer ways in which they can assist in the delivery of such initiatives. 

* UX Researchers should collaborate with Product Managers to determine the scope and priority of research studies.

### Working on a research study

1. Update the `Research proposal` with the following:
    * Label the issue with the area of GitLab you’re testing (for example, `navigation`), the status of the issue (`in progress`) and the Product stage (for example, `manage`).
    * Add a milestone to the issue.
    * Mark the issue as `confidential` until the research is completed so it doesn’t influence user behavior.
    * Assign the issue to yourself.
    * Add a checklist of actions that you plan to undertake. This makes it easier for people to understand where the research is up to.
    * Add related issue numbers.
1. Conduct the research. Ensure you keep the checklist up-to-date.
1. Schedule a wash-up meeting. 
    * Ensure you attach the study's key findings to the calendar invitation at least 24 hours prior to the meeting.
    * Add any supporting evidence (such as user videos) to the `Research proposal` issue.
1. Record and facilitate the wash-up meeting.
1. After the wash-up meeting, undertake any actions you have agreed to take forward, such as creating new issues.
1. Document the study within a [blog post](/handbook/marketing/blog/#create-a-new-post). The blog post should consist of:
    * Research hypotheses
    * Research methodology
    * Findings
    * Next steps/recommendations as agreed upon in the wash-up meeting.
1. Update the `Research proposal` with the following:
    * Link to the blog post.
    * Unmark the issue within the UX research project as `confidential`. (In some cases the issue may need to remain confidential if sensitive information is shared. If you’re unsure of whether an issue should remain confidential, please check with Sarah O’Donnell `@sarahod`).
    * Update the status of the issue to `done`.
    * Close the issue. You should stay assigned to closed issues so it's obvious who completed the research.

### Checklist templates

The following are examples of checklists that you may want to add to a `Research proposal`.

#### Usability Testing
* Schedule users. (Deadline:)
* Write script. (Deadline:)
* Test the script. Conduct 1 usability testing session. Edit the script if required. (Deadline:)
* Conduct remaining usability testing sessions. (Deadline:)
* Pay users. (Deadline:)
* Analyse videos. (Deadline:)
* Edit videos and add them to the `Research proposal` issue. (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the study's key findings to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Document the study's findings within a blog post. (Deadline:)
* Update the `Research proposal` issue. Link to the blog post. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### User Interviews
* Schedule users. (Deadline:)
* Write interview guide. (Deadline:)
* Conduct the interviews. (Deadline:)
* Pay users. (Deadline:)
* Analyse videos. (Deadline:)
* Edit videos and add them to the `Research proposal` issue. (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the study's key findings to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Document the study's findings within a blog post. (Deadline:)
* Update the `Research proposal` issue. Link to the blog post. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Surveys
* Write survey questions. (Deadline:)
* Import survey questions into SurveyMonkey. (Deadline:)
* Test survey logic. (Deadline:)
* Distribute survey to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the survey. (Deadline:)
* Distribute survey to remaining users. (Deadline:)
* Cleanse data and analyse survey results. (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the study's key findings to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Document the study's findings within a blog post. (Deadline:)
* Update the `Research proposal` issue. Link to the blog post. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Design Evaluations 
* Write instructions and/or questions.(Deadline:)
* Transfer instructions and/or questions into UsabilityHub. (Deadline:)
* Distribute study to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the study. (Deadline:)
* Distribute survey to remaining users. (Deadline:)
* Cleanse data and analyse responses. (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the study's key findings to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Document the study's findings within a blog post. (Deadline:)
* Update the `Research proposal` issue. Link to the blog post. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)

#### Card Sorts
* Write questions.(Deadline:)
* Set-up the study in OptimalWorkshop.
* Transfer questions into OptimalWorkshop. (Deadline:)
* Distribute study to a test sample of users. (Deadline:)
* Review answers and make any necessary amendments to the study. (Deadline:)
* Distribute study to remaining users. (Deadline:)
* Cleanse data and analyse responses. (Deadline:)
* Schedule a wash-up meeting. (Deadline:)
* Add the study's key findings to the wash-up meeting calendar invitation. (Deadline:)
* Record and facilitate the wash-up meeting. (Deadline:)
* Document the study's findings within a blog post. (Deadline:)
* Update the `Research proposal` issue. Link to the blog post. Unmark as `confidential` if applicable. Change status to `done` and close issue. (Deadline:)


### How to send a study to users of GitLab First Look.

Note: These instructions are for UX Researchers only. If you are not a UX Researcher but would like to send a study to users of GitLab First Look, please raise a [research proposal](/handbook/engineering/ux/ux-research//#how-to-request-research).

1. In MailChimp, create a new email campaign.
1. Click `Add recipients` under the `To` heading.
1. Choose `GitLab First Look` as your list.
1. Under `Segment or Tag` use the dropdown menu to select the segment of users you want to contact.
1. Leave the `From` field as is (emails are sent from `firstlook@gitlab.com`. Any emails sent to this address will forward to all UX Researchers).
1. Click `Add Subject`. Update the Subject field to: `Quick, new research study available!` and click `Save`.
1. Click `Design Email`. You are now going to design the content for your email campaign.
    * Click the `Saved templates` tab.
    * UX Research's templates are: `First Look - Survey`, `First Look - Usability Testing`, `First Look - User Interviews`, `First Look - Design Evaluations`, `First Look - Card Sort` and `First Look - Beta Testing`. Select the template you wish to use and click `Next`.
    * Review the content on the template, you will need to make some small alterations (such as researcher email address, dates of the study, etc). You can do this by clicking on the block of text you wish to change. This will bring up a WYSIWYG editor on the right hand side of your screen.
    * You will also need to update the CTA URL to your study URL. You can do this by clicking on the CTA. This will bring up a WYSIWYG editor on the right hand side of your screen where you can add a URL.
    * You should not make any styling changes to the templates. It's important that the design of emails from GitLab First Look stay consistent and are distinguishable from other email campaigns sent from GitLab.
1. Once you are ready to test your email campaign. Click `Preview and Test` in the top right corner of the screen.
    * `Enter preview mode` to see how your email will be displayed on Desktop and Mobile.
    * `Send a test email` to yourself. When the email arrives, double check the copy and any URLs.
1. Once you are happy with the design of your email campaign, click `Save & Close`.
1. You are now ready to send your email campaign! Click `Send` in the top right hand corner of the screen.
1. Move the campaign to the `UX Research` folder.


### Building a new segment in MailChimp

Segments are useful for setting up test pools of users, restricting the number of users who receive a study and for matching users to studies which are aligned with their interests.


1. In Mailchimp, navigate to `Lists` and click `GitLab First Look`.
1. Click `Manage Contacts` and select `Segments`.
1. Click `Create Segment`.
1. Update `any` to `all` so the statement reads: `Contacts match all of the following conditions:`
1. In the dropdown menu select any of the questions which appear under `Groups`. Once you have selected a question, MailChimp will automatically display a list of possible options. For example, if you select the question of: `What stages of DevOps lifecycle interest you?`, you will be given a list of options which relate to product stages (Plan, Create, Verify, etc).
1. Click `Add`
1. Repeat as necessary. You can add up to 5 conditions per segment. 
    * At the very minimum, you should always add conditions for:
        *  Research methodology (`What type of studies would you like to take part in?`)
        *  Product stage (`What type of studies would you like to take part in?`) or Meltano interest (`Would you like to receive studies about Meltano?`)
    * We want to personalise the studies we send to users. This means users only receive studies which are relevant to their interests. In turn, this helps GitLab First Look retain users and achieve a greater response rate to studies. Always consider what other conditions might be useful to your campaign. For example, it isn't useful for Core users to receive a study which asks for their feedback on an Ultimate feature.
1. Click `Preview Segment`. This will show you how many recipients match your conditions.
1. Click `Save Segment`.
1. Give the newly created segment a descriptive name.

### Incentives

* User interview or usability testing: $60 (or equivalent currency) Amazon Gift Card per 30 minutes. 
* Surveys or card sorts: Opportunity to win 1 of 3 $30 (or equivalent currency) Amazon Gift Cards.
* Beta testing: Unpaid.
* Design evaluations: Unpaid.

Amazon gift cards are country specific. When purchasing a gift card, ensure you use the appropriate Amazon store for a user's preferred country.

### Personas 

Existing personas are documented within the [GitLab Design System](https://design.gitlab.com/#/getting-started/personas). 

New personas or updates to existing personas can be added at any time. 

Personas should be:

* Informed by research.
* Driven by job title or feature.
* Gender neutral.
* Use bullet points and avoid long narrative.
* Use the [Jobs To Be Done framework](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done) 




[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
